from __future__ import print_function
import argparse
from math import log10
import numpy as np
import math

import os
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torchvision.utils as vutils
from torchvision.utils import make_grid
from torchvision.utils import save_image
import torchvision.transforms as transforms

from engine import *
from models import *
from util import *
from attr_only_iMat2020 import *

import gc
import json 

# Training settings
parser = argparse.ArgumentParser(description='PyTorch ML GCN Attribute classifier')
parser.add_argument('--data', type=str, default='./', help='val data')
parser.add_argument('--batch_size', type=int, default=16, help='training batch size')
parser.add_argument('--lr', type=float, default=0.01, help='Learning Rate. Default=0.0001')
parser.add_argument('--cpu', default=False, action='store_true', help='Use CPU to test')
parser.add_argument('--threads', type=int, default=1, help='number of threads for data loader to use')
parser.add_argument('--seed', type=int, default=67454, help='random seed to use. Default=123')
parser.add_argument('--gpus', default=1, type=int, help='number of gpu')
parser.add_argument('--threshold', type=float, default=0.5)
parser.add_argument('--model', default='/media/chintu/bharath_ext_hdd/Bharath/Attribute Classifier/ML-GCN-master/checkpoint/coco/checkpoint.pth.tar', help='pretrained base model')
parser.add_argument('--save', default=False, action='store_true', help='If save test images')
parser.add_argument('--save_path', type=str, default='./results/')
parser.add_argument('--workers', type=int, default=25, help='workers')
parser.add_argument('--image_size', type=int, default=448, help='image size')

opt = parser.parse_args()

final_output=[]

def eval():
       
    print("Evaluating....")
    model.eval()
    count = 1
    
    for index,batch in enumerate(val_loader):
        print("eval", index)
        input, target = batch
        
        img= input[0]
        file_name=input[1]
        vectors = input[2]
        
        # print("target",type(target), len(target))
        
        print("img", type(img), img.shape)
        
        if cuda:
            img = img.cuda()
            vectors = vectors.cuda()
            # print(vectors, type(vectors))
            # input = input.cuda()
        
        feature_var = torch.autograd.Variable(img).float()
        
        inp_var = torch.autograd.Variable(vectors).float().detach()  # one hot
        
    
        with torch.no_grad():
            prediction = model(feature_var, inp_var)

        prediction = prediction.to(torch.device('cpu'))
        target = target.to(torch.device('cpu'))

        # print("target",target)
        # print("before prediction", prediction)
        prediction[prediction<0]=0
        prediction[prediction>0]=1
        
        # print("after prediction",prediction)

        target[target<0]=0
        target[target>0]=1

        
        from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
        #calculate accuracy, precision and recall
        print('F1: {}'.format(f1_score(target, prediction > 0.5, average="samples")))
        print('precision: {}'.format(precision_score(target, prediction, average="samples")))
        print('recall: {}'.format(recall_score(target, prediction, average="samples")))
        # print('Accuracy: {}'.format(accuracy_score(target, prediction, normalize=True, sample_weight=None)))
        
        for i in range(len(file_name)):
            # print("i",i, file_name[i])
            output = {}
            output['file_name'] = file_name[i]
            output['target'] = np.array(target[i])
            output['pred'] = np.array(prediction[i])
            
            final_output.append(output)
        count+=1

        # print("count", count)


    category = json.load(open('./data/iMat/attributes.json','r'))

        

    # print('final_output', type(final_output), len(final_output))
    import pickle 

    with open('attr_only_final_output.pkl','wb') as outfile:
        pickle.dump(final_output, outfile)

if __name__ == '__main__':
    if opt.cpu:
        print("===== Use CPU to Test! =====")
    else:
        print("===== Use GPU to Test! =====")


    val_dataset = iMat2020(opt.data, phase='val', inp_name='./data/iMat/attr_only_iMat_glove_word2vec.pkl')
    # print("val d", val_dataset)

    
    num_classes=294
    
    ## Set the GPU mode
    gpus_list=range(opt.gpus)
    cuda = not opt.cpu
    if cuda and not torch.cuda.is_available():
        raise Exception("No GPU found, please run without --cuda")


    # Model
    # model = InpaintingModel(g_lr=opt.lr, d_lr=(0.1 * opt.lr), l1_weight=opt.l1_weight, gan_weight=opt.gan_weight, iter=0, threshold=opt.threshold)
    model = gcn_resnet101(num_classes=num_classes, t=0.2, adj_file='./data/iMat/attr_only_iMat_corr_matrix.pkl')


    # print('---------- Networks architecture -------------')
    # print_network(model)
    # print('----------------------------------------------')

    pretained_model = torch.load(opt.model, map_location=lambda storage, loc: storage)
    
    
    # optionally resume from a checkpoint

    checkpoint = torch.load(opt.model)
    start_epoch = checkpoint['epoch']
    best_score = checkpoint['best_score']
    try:
        model.load_state_dict(checkpoint['state_dict'])

    except:
        state_dict =checkpoint['state_dict']
        from collections import OrderedDict
        new_state_dict = OrderedDict()

        for k, v in state_dict.items():
            if 'module' not in k:
                k = 'module.'+k
            else:
                k = k.replace('features.module.', 'module.features.')
            new_state_dict[k]=v

        model.load_state_dict(new_state_dict)
    
    
    print("=> loaded checkpoint (epoch {}) with best_score: {}".format(checkpoint['epoch'], best_score))
    
    use_gpu = torch.cuda.is_available()
    cuda = torch.cuda.is_available()
    

    if cuda:
        model = model.cuda()
        if opt.gpus > 1:
            print("gpus more than 1", opt.gpus)
            model = torch.nn.DataParallel(model, device_ids=opt.device_ids).cuda()


    print('Pre-trained G model is loaded.')

    
    normalize = transforms.Normalize(mean=model.image_normalization_mean,
                                        std=model.image_normalization_std)
    val_transform = transforms.Compose([Warp(opt.image_size),
                                        transforms.ToTensor(),normalize,])
    val_dataset.transform = val_transform
    # val_dataset.target_transform = val_target_transform
    
    
    val_loader = torch.utils.data.DataLoader(val_dataset,
                                                 batch_size=opt.batch_size, shuffle=False,
                                                 num_workers=opt.workers)

    # print("val_loader", len(val_loader))
    # if use_gpu:
    #     val_loader.pin_memory = True
    #     cudnn.benchmark = True
    ## Eval Start!!!!
    print('===> Loaded datasets')

    eval()


