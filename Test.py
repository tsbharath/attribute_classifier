from __future__ import print_function
import argparse
import numpy as np

import os
import torch
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from engine import *
from models import *
from util import *
from iMat2020 import *

import gc
import json 
import pandas as pd
import pickle

# Test 
parser = argparse.ArgumentParser(description='PyTorch ML GCN Attribute classifier')
parser.add_argument('--data', type=str, default='./', help='val data')
parser.add_argument('--batch_size', type=int, default=16, help='training batch size')
parser.add_argument('--cpu', default=False, action='store_true', help='Use CPU to test')
parser.add_argument('--threads', type=int, default=1, help='number of threads for data loader to use')
parser.add_argument('--seed', type=int, default=67454, help='random seed to use. Default=123')
parser.add_argument('--gpus', default=1, type=int, help='number of gpu')
parser.add_argument('--model', default='./checkpoint/coco/checkpoint.pth.tar', help='pretrained base model')
parser.add_argument('--save', default=False, action='store_true', help='If save test images')
parser.add_argument('--save_path', type=str, default='./results/')
parser.add_argument('--workers', type=int, default=25, help='workers')
parser.add_argument('--image_size', type=int, default=448, help='image size')

opt = parser.parse_args()

final_output=[]
trans = transforms.ToTensor()

def eval():
       
    print("Evaluating....")
    model.eval()
    count = 1
    
    
    for index,batch in enumerate(test_data):
        print("eval", index)
        input = batch
        
        img= input[0]
        file_name=input[1]
        
        img = test_transform(img)
        img = img.unsqueeze(1) 
        img = img.permute(1, 0, 2, 3) 
        
        if cuda:
            img = img.cuda()
        
        feature_var = torch.autograd.Variable(img).float()      
        inp_var = torch.autograd.Variable(vectors).float().detach()  # one hot
        
        with torch.no_grad():
            prediction = model(feature_var, inp_var)

        prediction = prediction.to(torch.device('cpu'))
        
        prediction[prediction<0]=0
        prediction[prediction>0]=1
        
        
        output = {}
        output['file_name'] = file_name
        output['pred'] = np.array(prediction[0])      
        final_output.append(output)
        
        count+=1


    category = json.load(open('./data/iMat/category.json','r'))

    catg_val = {} 
    for k,v in category.items():
        catg_val[v]=k

    final_output_df = pd.DataFrame(final_output)

    final_output_df['pred_ind'] = final_output_df['pred'].apply(lambda x: [i if e == 1 else None for i, e in enumerate(x) ])
    final_output_df['pred_val'] = final_output_df['pred_ind'].apply(lambda x: [catg_val[i] if i!=None else 0 for i in x ])  
    final_output_df['pred_items'] = final_output_df['pred_val'].apply(lambda x: list(set(x)))

    
    with open('final_output_test.pkl','wb') as outfile:
        pickle.dump(final_output_df, outfile)

        

if __name__ == '__main__':

    
    num_classes=340
    
    
    if opt.cpu:
        print("===== Use CPU to Test! =====")
    else:
        print("===== Use GPU to Test! =====")

    test_data=[]
    
    print("Number of test images", len(os.listdir(opt.data)))

    for i in os.listdir(opt.data):
         
        filename = os.path.basename(i)
        img = Image.open(os.path.join(opt.data, '{}'.format(filename))).convert('RGB')
        test_data.append((img, filename)) 
    

    
    vectors= pickle.load(open('./data/iMat/iMat_glove_word2vec.pkl','rb'))
    vectors = trans(vectors)
    
    ## Set the GPU mode
    gpus_list=range(opt.gpus)
    cuda = not opt.cpu
    if cuda and not torch.cuda.is_available():
        raise Exception("No GPU found, please run without --cuda")


    # Model
    model = gcn_resnet101(num_classes=num_classes, t=0.4, adj_file='./data/iMat/iMat_corr_matrix.pkl')
    pretained_model = torch.load(opt.model, map_location=lambda storage, loc: storage)
    
    
    # optionally resume from a checkpoint

    checkpoint = torch.load(opt.model)
    start_epoch = checkpoint['epoch']
    best_score = checkpoint['best_score']
    try:
        model.load_state_dict(checkpoint['state_dict'])

    except:
        state_dict =checkpoint['state_dict']
        from collections import OrderedDict
        new_state_dict = OrderedDict()

        for k, v in state_dict.items():
            if 'module' not in k:
                k = 'module.'+k
            else:
                k = k.replace('features.module.', 'module.features.')
            new_state_dict[k]=v

        model.load_state_dict(new_state_dict)
    
    
    print("=> loaded checkpoint (epoch {}) with best_score: {}".format(checkpoint['epoch'], best_score))
    
    use_gpu = torch.cuda.is_available()
    cuda = torch.cuda.is_available()
    

    if cuda:
        model = model.cuda()
        vectors = vectors.cuda()
        if opt.gpus > 1:
            print("gpus more than 1", opt.gpus)
            model = torch.nn.DataParallel(model, device_ids=opt.device_ids).cuda()


    print('Pre-trained G model is loaded.')

    
    normalize = transforms.Normalize(mean=model.image_normalization_mean,
                                        std=model.image_normalization_std)

    test_transform = transforms.Compose([Warp(opt.image_size),
                                        transforms.ToTensor(),normalize,])

    # test_data.transform = test_transform    

    # test_loader = torch.utils.data.DataLoader(test_data,
    #                                              batch_size=opt.batch_size, shuffle=False,
    #                                              num_workers=opt.workers)

    
    ## Eval Start!!!!
    print('===> Loaded datasets')

    eval()


