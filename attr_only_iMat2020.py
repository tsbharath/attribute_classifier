import torch.utils.data as data
import json
import os
import subprocess
from PIL import Image
import numpy as np
import torch
import pickle
from util import *



class attr_only_iMat2020(data.Dataset):
    def __init__(self, root, transform=None, phase='train', inp_name=None):
        self.root = root
        self.phase = phase
        self.img_list = []
        self.transform = transform
        # download_coco2014(root, phase)
        self.get_anno()
        self.num_classes = len(self.cat2idx)

        with open(inp_name, 'rb') as f:
            self.inp = pickle.load(f)
        self.inp_name = inp_name

    def get_anno(self):
        list_path = os.path.join(self.root, 'data/iMat/', 'attr_only_{}_anno_2020.json'.format(self.phase))
        self.img_list = json.load(open(list_path, 'r'))
        self.img_list = self.img_list #[0:100]
        self.cat2idx = json.load(open(os.path.join(self.root, 'data/iMat/', 'attributes.json'), 'r'))


    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, index):
        item = self.img_list[index]
        return self.get(item)

    def get(self, item):
        filename = item['file_name']
        labels = sorted(item['labels'])
        img = Image.open(os.path.join('/media/chintu/bharath_ext_hdd/Bharath/Attribute Classifier/train', '{}.jpg'.format(filename))).convert('RGB')
        if self.transform is not None:
            img = self.transform(img)
        target = np.zeros(self.num_classes, np.float32) -1
        target[labels] = 1
        return (img, filename, self.inp), target



class load_testdata(data.Dataset):
    def __init__(self, root, transform=None, inp_name=None):
        self.root = root
        print(self.root)
        self.img_list = []
        self.transform = transform
        self.get_anno()
        self.num_classes = len(self.cat2idx)

        with open(inp_name, 'rb') as f:
            self.inp = pickle.load(f)
        self.inp_name = inp_name

    def get_anno(self):
        # list_path = os.path.join(self.root, 'data/iMat/', '{}_anno_2020.json'.format(self.phase))
        # self.img_list = json.load(open(list_path, 'r'))
        self.img_list = os.listdir(self.root)
        self.cat2idx = json.load(open(os.path.join('./data/iMat/', 'attributes.json'), 'r'))


    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, index):
        item = self.img_list[index]
        return self.get(item)

    def get(self, item):
        filename = item
        img = Image.open(os.path.join(self.root, '{}'.format(filename))).convert('RGB')
        if self.transform is not None:
            img = self.transform(img)
        return (img, filename, self.inp)