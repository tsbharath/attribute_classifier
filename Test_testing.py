from __future__ import print_function
import argparse
import numpy as np

import os
import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from engine import *
from models import *
from util import *
from iMat2020 import *

import gc
import json 
import pandas as pd
import pickle


# Test 
# parser = argparse.ArgumentParser(description='PyTorch ML GCN Attribute classifier')
# parser.add_argument('--data', type=str, default='./data/test/', help='val data')
# parser.add_argument('--batch_size', type=int, default=16, help='training batch size')
# parser.add_argument('--cpu', default=False, action='store_true', help='Use CPU to test')
# parser.add_argument('--gpus', default=1, type=int, help='number of gpu')
# parser.add_argument('--model', default='./checkpoint/coco/checkpoint.pth.tar', help='pretrained base model')
# parser.add_argument('--workers', type=int, default=25, help='workers')
# parser.add_argument('--image_size', type=int, default=448, help='image size')

# opt = parser.parse_args()


# class Get_attribute(str):



def predict(data_loc=None):

    global cuda,test_loader, final_output, model
    
    data = data_loc
    batch_size = 2
    cpu=True
    gpus=1
    model_loc = './checkpoint/coco/checkpoint.pth.tar'
    workers = 25
    image_size=448
    
    num_classes=340
    
    if cpu:
        print("===== Use CPU to Test! =====\n")
    else:
        print("===== Use GPU to Test! =====\n")

    gpus_list=range(gpus)
    cuda = not cpu
    if cuda and not torch.cuda.is_available():
        raise Exception("No GPU found, please run without --cuda \n")


    test_dataset = load_testdata(data, inp_name='./data/iMat/iMat_glove_word2vec.pkl')

    # Model
    model = gcn_resnet101(num_classes=num_classes, t=0.4, adj_file='./data/iMat/iMat_corr_matrix.pkl')
    pretained_model = torch.load(model_loc, map_location=lambda storage, loc: storage)
    
    
    # optionally resume from a checkpoint

    checkpoint = torch.load(model_loc)
    start_epoch = checkpoint['epoch']
    best_score = checkpoint['best_score']
    try:
        model.load_state_dict(checkpoint['state_dict'])

    except:
        state_dict =checkpoint['state_dict']
        from collections import OrderedDict
        new_state_dict = OrderedDict()

        for k, v in state_dict.items():
            if 'module' not in k:
                k = 'module.'+k
            else:
                k = k.replace('features.module.', 'module.features.')
            new_state_dict[k]=v

        model.load_state_dict(new_state_dict)
    
    
    print("=> loaded checkpoint (epoch {}) with best_score: {}\n".format(checkpoint['epoch'], best_score))
    
    # use_gpu = torch.cuda.is_available()
    cuda = torch.cuda.is_available()
    
    if cuda:
        model = model.cuda()
        if gpus > 1:
            # print("gpus more than 1", gpus)
            model = torch.nn.DataParallel(model, device_ids=device_ids).cuda()

    print('Pre-trained G model is loaded.\n')

    
    normalize = transforms.Normalize(mean=model.image_normalization_mean,
                                        std=model.image_normalization_std)

    test_transform = transforms.Compose([Warp(image_size),
                                        transforms.ToTensor(),normalize,])

    test_dataset.transform = test_transform    

    test_loader = torch.utils.data.DataLoader(test_dataset,
                                                batch_size=batch_size, shuffle=False,
                                                num_workers=workers)

    
    ## Eval Start!!!!
    print('===> Loaded datasets\n')
    final_output=[]  
    
    eval_model()

def eval_model():

    print("Evaluating....")
    model.eval()
    count = 1
    
    for index,batch in enumerate(test_loader):
        
        input = batch
        
        img= input[0]
        file_name=input[1]
        vectors = input[2]
        
        
        if cuda:
            img = img.cuda()
            vectors = vectors.cuda()


        feature_var = torch.autograd.Variable(img).float()
        inp_var = torch.autograd.Variable(vectors).float().detach()
        
    
        with torch.no_grad():
            prediction = model(feature_var, inp_var)

        prediction = prediction.to(torch.device('cpu'))

        prediction[prediction<0]=0
        prediction[prediction>0]=1
        
        
        for i in range(len(file_name)):
            output = {}
            output['file_name'] = file_name[i].split('.')[0]
            output['pred'] = np.array(prediction[i])
            final_output.append(output)

    category = json.load(open('./data/iMat/category.json','r'))
    catg_val = {} 
    for k,v in category.items():
        catg_val[v]=k

    final_output_df = pd.DataFrame(final_output)
    final_output_df['pred_ind'] = final_output_df['pred'].apply(lambda x: [i if e == 1 else None for i, e in enumerate(x) ])
    final_output_df['pred_val'] = final_output_df['pred_ind'].apply(lambda x: [catg_val[i] if i!=None else 0 for i in x ])  
    final_output_df['pred_items'] = final_output_df['pred_val'].apply(lambda x: list(set(x))[1:])
    final_output_df = final_output_df[['file_name','pred_items']]
    final_output_df.columns = ['Image','Predicted_Classes']
    
    with open('Predicted_clases.pkl','wb') as outfile:
        pickle.dump(final_output_df, outfile)
    
    print("\nModel output : Displaying First 20 records\n")
    
    if (len(test_loader)*len(batch)) > 20 :
        final_output_df = final_output_df[0:20]

    from IPython.display import display, HTML
    display(HTML(final_output_df.to_html()))
