import torch.utils.data as data
import json
import os
import subprocess
from PIL import Image
import numpy as np
import torch
import pickle
from util import *



class iMat2020(data.Dataset):
    def __init__(self, root, transform=None, phase='train', inp_name=None):
        self.root = root
        self.phase = phase
        self.img_list = []
        self.transform = transform
        # download_coco2014(root, phase)
        self.get_anno()
        self.num_classes = len(self.cat2idx)

        with open(inp_name, 'rb') as f:
            self.inp = pickle.load(f)
        self.inp_name = inp_name

    def get_anno(self):
        list_path = os.path.join(self.root, 'data/iMat/', 'seg_{}_anno_2020.json'.format(self.phase))
        self.img_list = json.load(open(list_path, 'r'))
        self.img_list = self.img_list
        self.cat2idx = json.load(open(os.path.join(self.root, 'data/iMat/', 'category.json'), 'r'))


    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, index):
        item = self.img_list[index]
        return self.get(item)


    def rle_decode(self,mask_rle, shape):
        s = mask_rle
        starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
        starts -= 1
        ends = starts + lengths
        img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
        for lo, hi in zip(starts, ends):
            img[lo:hi] = 1
        return img.reshape(shape)


    def get(self, item):
        
        labels = sorted(item['labels'])
        filename = item['file_name']
        
        im1 = Image.open(os.path.join('/media/chintu/bharath_ext_hdd/Bharath/Attribute Classifier/train', '{}.jpg'.format(filename.split('_')[1]))).convert('RGB')
        pixels = item['EncodedPixels'].split()
        IMG_SHAPE=(int(item['Width']),int(item['Height']))
        
        all_masks = np.zeros(IMG_SHAPE)
        all_masks += self.rle_decode(pixels, shape=IMG_SHAPE)
        mask1 = np.logical_not(all_masks.T).astype(int)*256
        img = mask1[:,:,None]+np.array(im1)
        img[img>=256]=255
        img = Image.fromarray(np.uint8(img))

        if self.transform is not None:
            img = self.transform(img)
        target = np.zeros(self.num_classes, np.float32) -1
        target[labels] = 1
        del im1, pixels, all_masks, mask1
        
        return (img, filename, self.inp), target



class load_testdata(data.Dataset):
    def __init__(self, root, transform=None, inp_name=None):
        self.root = root
        print(self.root)
        self.img_list = []
        self.transform = transform
        self.get_anno()
        self.num_classes = len(self.cat2idx)

        with open(inp_name, 'rb') as f:
            self.inp = pickle.load(f)
        self.inp_name = inp_name

    def get_anno(self):
        # list_path = os.path.join(self.root, 'data/iMat/', '{}_anno_2020.json'.format(self.phase))
        # self.img_list = json.load(open(list_path, 'r'))
        self.img_list = os.listdir(self.root)
        self.cat2idx = json.load(open(os.path.join('./data/iMat/', 'category.json'), 'r'))


    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, index):
        item = self.img_list[index]
        return self.get(item)

    def get(self, item):
        filename = item
        img = Image.open(os.path.join(self.root, '{}'.format(filename))).convert('RGB')
        if self.transform is not None:
            img = self.transform(img)
        return (img, filename, self.inp)